package queue;


public interface HelpDeskQueue {

    void enqueue(HelpDeskItem helpDeskItem);

    HelpDeskItem dequeue();

    void overviewByPriority();

    void overviewNatural();
}
